from pathlib import Path
import os

class Config:
    APP_NAME = "APP_NAME"
    DEBUG = False
    TESTING = False

    SECRET_KEY = "Change this to something secure"
    APP_ROOT = os.path.join(Path(__file__).parent, "app")

    DB_NAME = "app.db"
    DB_USERNAME = ""
    DB_PASSWORD = ""
    SQLALCHEMY_DATABASE_URI = f"sqlite:///{DB_NAME}"
    SQLALCHEMY_TRACK_MODIFICATIONS = False
    HOST = "127.0.0.1"


    UPLOAD_FOLDER = f"{APP_ROOT}/static/generated_audio"

class DevelopmentConifg(Config):
    DEBUG = True
    
class ProductionConfig(Config):
    HOST = "0.0.0.0"

class TestingConfig(Config):
    TESTING = True
