from flask import render_template

from . import app

@app.route("/", methods=["GET"])
def index():
    return render_template("pages/index.html")
